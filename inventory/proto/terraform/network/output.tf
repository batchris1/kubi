# -----------------------------------------------------------------------------
# VPC OUTPUTS
# -----------------------------------------------------------------------------

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "vpc_cidr" {
  value = "${var.vpc_cidr}"
}

# -----------------------------------------------------------------------------
# SUBNET OUTPUTS
# -----------------------------------------------------------------------------
output "public_subnet_id_a" {
  value = "${module.subnets_az_a.public_subnet_id}"
}

output "public_subnet_cidr_a" {
  value = "${module.subnets_az_a.public_subnet_cidr}"
}

output "private_subnet_id_a" {
  value = "${module.subnets_az_a.private_subnet_id}"
}

output "private_subnet_cidr_a" {
  value = "${module.subnets_az_a.private_subnet_cidr}"
}

# -----------------------------------------------------------------------------

output "public_subnet_id_b" {
  value = "${module.subnets_az_b.public_subnet_id}"
}

output "public_subnet_cidr_b" {
  value = "${module.subnets_az_b.public_subnet_cidr}"
}

output "private_subnet_id_b" {
  value = "${module.subnets_az_b.private_subnet_id}"
}

output "private_subnet_cidr_b" {
  value = "${module.subnets_az_b.private_subnet_cidr}"
}

# ------------------------------------------------------------------------------

output "public_subnet_id_c" {
  value = "${module.subnets_az_c.public_subnet_id}"
}

output "public_subnet_cidr_c" {
  value = "${module.subnets_az_c.public_subnet_cidr}"
}

output "private_subnet_id_c" {
  value = "${module.subnets_az_c.private_subnet_id}"
}

output "private_subnet_cidr_c" {
  value = "${module.subnets_az_c.private_subnet_cidr}"
}

# ------------------------------------------------------------------------------
# ROUTE TABLE OUTPUTS
# ------------------------------------------------------------------------------
output "main_route_table_id" {
  value = "${module.main_route_table.route_table_id}"
}

output "private_route_table_id" {
  value = "${module.private_route_table.route_table_id}"
}

output "init_tags" {
  value = {
    name  = "${var.basename}"
    env   = "${var.environment}"
    owner = "${var.owner}"
    stack = "${var.stack}"
  }
}

output "public_subnets_id_list" {
  value = [
    "${module.subnets_az_a.public_subnet_id}",
    "${module.subnets_az_b.public_subnet_id}",
    "${module.subnets_az_c.public_subnet_id}"
  ]
}

output "private_subnets_id_list" {
  value = [
    "${module.subnets_az_a.private_subnet_id}",
    "${module.subnets_az_b.private_subnet_id}",
    "${module.subnets_az_c.private_subnet_id}"
  ]
}

output "az_list" {
  value = "${var.az_list}"
}