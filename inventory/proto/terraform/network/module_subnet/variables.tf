variable "vpc_id" {}

variable "public_subnet_cidr" {}

variable "private_subnet_cidr" {}

variable "availability_zone" {}

variable "environment" {}
variable "owner" {}
variable "stack" {}

#variable "public_gateway_route_table_id" {}

variable "basename" {
  type = "string"
}
