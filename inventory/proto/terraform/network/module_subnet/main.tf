# --------------------------------------
# PUBLIC SUBNET
# --------------------------------------
resource "aws_subnet" "public_subnet" {
  vpc_id = "${var.vpc_id}"

  cidr_block = "${var.public_subnet_cidr}"

  availability_zone = "${var.availability_zone}"

  tags {
    Name        = "${var.basename}_${var.environment}_pub_${var.availability_zone}"
    environment = "${var.environment}"
    owner       = "${var.owner}"
    stack       = "${var.stack}"
  }
}

# ---------------------------------------
# PRIVATE SUBNET
# ---------------------------------------

resource "aws_subnet" "private_subnet" {
  vpc_id = "${var.vpc_id}"

  cidr_block = "${var.private_subnet_cidr}"

  availability_zone = "${var.availability_zone}"

  tags {
    Name        = "${var.basename}_${var.environment}_priv_${var.availability_zone}"
    environment = "${var.environment}"
    owner       = "${var.owner}"
    stack       = "${var.stack}"
  }
}
