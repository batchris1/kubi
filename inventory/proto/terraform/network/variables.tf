variable "vpc_cidr" {
  type    = "string"
  default = "10.42.0.0/16"
}

variable "aws_region" {
  type    = "string"
  default = "eu-west-1"
}

provider "aws" {
  region = "${var.aws_region}"
}

variable "az_list" {
  type    = "list"
  default = ["a", "b", "c"]
}

variable "environment" {
  type    = "string"
  default = "demo"
}

variable "owner" {
  type    = "string"
  default = "it-crowd"
}

variable "stack" {
  type    = "string"
  default = "kubi"
}

variable "basename" {
  type    = "string"
  default = "proto"
}
