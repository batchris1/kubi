# -----------------------------------------------------------------------------
# CREATE A VPC
# -----------------------------------------------------------------------------
resource "aws_vpc" "vpc" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name        = "${var.basename}-${var.environment}"
    Environment = "${var.environment}"
    Owner       = "${var.owner}"
    Stack       = "${var.stack}"
  }
}

# -----------------------------------------------------------------------------
# CREATE MAON ROUTE TABLE
# -----------------------------------------------------------------------------

module "main_route_table" {
  source      = "./module_route_table"
  basename    = "${var.basename}"
  mode        = "public"
  environment = "${var.environment}"
  owner       = "${var.owner}"
  stack       = "${var.stack}"
  vpc_id      = "${aws_vpc.vpc.id}"
}

# -----------------------------------------------------------------------------
# CREATE PRIVATE ROUTE TABLE
# -----------------------------------------------------------------------------

module "private_route_table" {
  source      = "./module_route_table"
  basename    = "${var.basename}"
  mode        = "private"
  environment = "${var.environment}"
  owner       = "${var.owner}"
  stack       = "${var.stack}"
  vpc_id      = "${aws_vpc.vpc.id}"
}

# -----------------------------------------------------------------------------
# CREATE SUBNETS IN DIFFERENT AZs
# -----------------------------------------------------------------------------

module "subnets_az_a" {
  source      = "./module_subnet"
  basename    = "${var.basename}"
  environment = "${var.environment}"
  owner       = "${var.owner}"
  stack       = "${var.stack}"

  availability_zone   = "${var.aws_region}a"
  vpc_id              = "${aws_vpc.vpc.id}"
  public_subnet_cidr  = "${cidrsubnet(var.vpc_cidr, 8, 1)}"
  private_subnet_cidr = "${cidrsubnet(var.vpc_cidr, 6, 4)}"

  #public_gateway_route_table_id = "${module.main_route_table.route_table_id}"
}

module "subnets_az_b" {
  source      = "./module_subnet"
  basename    = "${var.basename}"
  environment = "${var.environment}"
  owner       = "${var.owner}"
  stack       = "${var.stack}"

  availability_zone   = "${var.aws_region}b"
  vpc_id              = "${aws_vpc.vpc.id}"
  public_subnet_cidr  = "${cidrsubnet(var.vpc_cidr, 8, 2)}"
  private_subnet_cidr = "${cidrsubnet(var.vpc_cidr, 6, 6)}"

  #public_gateway_route_table_id = "${module.main_route_table.route_table_id}"
}

module "subnets_az_c" {
  source      = "./module_subnet"
  basename    = "${var.basename}"
  environment = "${var.environment}"
  owner       = "${var.owner}"
  stack       = "${var.stack}"

  availability_zone   = "${var.aws_region}c"
  vpc_id              = "${aws_vpc.vpc.id}"
  public_subnet_cidr  = "${cidrsubnet(var.vpc_cidr, 8, 3)}"
  private_subnet_cidr = "${cidrsubnet(var.vpc_cidr, 6, 8)}"

  #public_gateway_route_table_id = "${module.main_route_table.route_table_id}"
}

# -----------------------------------------------------------------------------
# CREATE A INTERNET GATEWAY
# -----------------------------------------------------------------------------

resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.basename}_${var.environment}_igw"
    Environment = "${var.environment}"
    Owner       = "${var.owner}"
    Stack       = "${var.stack}"
  }

  //  lifecycle {
  //    prevent_destroy = true
  //  }
}

# -----------------------------------------------------------------------------
# CREATE A NAT GATEWAY
# -----------------------------------------------------------------------------
module "nat_gateway" {
  source      = "./module_nat"
  basename    = "${var.basename}"
  environment = "${var.environment}"
  owner       = "${var.owner}"
  stack       = "${var.stack}"
  subnet_id_a = "${module.subnets_az_a.public_subnet_id}"
}

# -----------------------------------------------------------------------------
# ATTACH IGW TO PUBLIC ROUTE TABLES
# -----------------------------------------------------------------------------

module "attach_route_igw" {
  source         = "./module_route_table/module_create_route_to_igw"
  route_table_id = "${module.main_route_table.route_table_id}"
  igw_id         = "${aws_internet_gateway.gateway.id}"
  subnets_list   = "${list(module.subnets_az_a.public_subnet_id,module.subnets_az_b.public_subnet_id,module.subnets_az_c.public_subnet_id )}"
  destination    = "0.0.0.0/0"
}

# -----------------------------------------------------------------------------
# ATTACH NAT TO PRIVATE ROUTE TABLES
# -----------------------------------------------------------------------------


module "attach_route_nat" {
  source         = "./module_route_table/module_create_route_to_nat"
  route_table_id = "${module.private_route_table.route_table_id}"
  nat_id         = "${module.nat_gateway.nat_id}"
  subnets_list   = "${list(module.subnets_az_a.private_subnet_id,module.subnets_az_b.private_subnet_id,module.subnets_az_c.private_subnet_id )}"
  destination    = "0.0.0.0/0"
}

