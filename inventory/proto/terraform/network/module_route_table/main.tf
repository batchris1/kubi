# -----------------------------------------------------------------------------
# CREATE A ROUTE TABLE? ROUTE AND ASSOCIATION TO SUBNET
# -----------------------------------------------------------------------------

resource "aws_route_table" "route_table" {
  vpc_id = "${var.vpc_id}"

  tags {
    Name        = "${var.basename}_${var.environment}_${var.mode}"
    Environment = "${var.environment}"
    Owner       = "${var.owner}"
    Stack       = "${var.stack}"
  }

  //  lifecycle {
  //    prevent_destroy = true
  //  }
}
