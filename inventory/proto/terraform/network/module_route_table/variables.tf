variable "vpc_id" {}
variable "mode" {}
variable "environment" {}
variable "owner" {}
variable "stack" {}

variable "basename" {
  type = "string"
}
