# could be improved to take a dictionary of source, destinations
resource "aws_route" "route_igw" {
  route_table_id         = "${var.route_table_id}"
  destination_cidr_block = "${var.destination}"
  gateway_id             = "${var.igw_id}"

  //  lifecycle {
  //    prevent_destroy = true
  //  }
}

resource "aws_route_table_association" "route_table_association" {
  //  count = "${length(var.subnets_list)}"
  count          = 3
  route_table_id = "${var.route_table_id}"
  subnet_id      = "${element(var.subnets_list, count.index)}"

  //  lifecycle {
  //    prevent_destroy = true
  //  }
}
