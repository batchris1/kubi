variable "route_table_id" {}
variable "igw_id" {}

variable "subnets_list" {
  type = "list"
}

variable "destination" {}
