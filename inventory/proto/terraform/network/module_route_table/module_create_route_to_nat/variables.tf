variable "route_table_id" {}
variable "nat_id" {}

variable "subnets_list" {
  type = "list"
}

variable "destination" {}
