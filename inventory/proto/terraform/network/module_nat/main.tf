# -----------------------------------------------------------------------------
# NAT GATEWAY in AZ A
# -----------------------------------------------------------------------------

resource "aws_eip" "nat_eip" {}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${var.subnet_id_a}"

  tags {
    Name        = "${var.basename}_${var.environment}_nat"
    Environment = "${var.environment}"
    Owner       = "${var.owner}"
    Stack       = "${var.stack}"
  }
}
