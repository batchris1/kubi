variable "basename" {
  type = "string"
}

variable "environment" {
  type = "string"
}

variable "owner" {
  type = "string"
}

variable "stack" {
  type = "string"
}

variable subnet_id_a {
  type = "string"
}
