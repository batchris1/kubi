data "terraform_remote_state" "network" {
  backend = "local"

  config {
    path = "../network/terraform.tfstate"
  }
}

variable "private_key_path" {
  type    = "string"
  default = "../../proto.master-key"
}

variable "aws_region" {
  type    = "string"
  default = "eu-west-1"
}

provider "aws" {
  region = "${var.aws_region}"
}


variable "public_key_path" {
  type    = "string"
  default = "../../proto.master-key.pub"
}

variable "instance_type" {
  default = "m4.large"
}

variable "input_instance_type" {
  default = "m4.xlarge"
}

variable "output_instance_type" {
  default = "m4.2xlarge"
}

variable "ceph_volume_mapping" {
  default = "/dev/xvdb"
}

variable "ceph_volume_size" {
  default = "200"
}



data "aws_ami" "centos" {
  most_recent = true
  owners      = ["679593333241"]

  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

locals {
  az_list = "${data.terraform_remote_state.network.az_list}"
  init_tags = {
    basename  = "${data.terraform_remote_state.network.init_tags.name}"
    name  = "${data.terraform_remote_state.network.init_tags.name}-${data.terraform_remote_state.network.init_tags.env}"
    env   = "${data.terraform_remote_state.network.init_tags.env}"
    owner = "${data.terraform_remote_state.network.init_tags.owner}"
    stack = "${data.terraform_remote_state.network.init_tags.stack}"
  }
}
