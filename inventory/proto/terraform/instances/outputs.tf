output "ansible_inventory" {
  value = <<EOF

input-0 ansible_host=${aws_instance.input_host.private_ip}
output-0 ansible_host=${aws_instance.output_host.private_ip}
repository-0 ansible_host=${aws_instance.repository_host.private_ip}

master-0 ansible_host=${aws_instance.kube_master.0.private_ip}
master-1 ansible_host=${aws_instance.kube_master.1.private_ip}
master-2 ansible_host=${aws_instance.kube_master.2.private_ip}

node-0 ansible_host=${aws_instance.kube_node.0.private_ip}
node-1 ansible_host=${aws_instance.kube_node.1.private_ip}
node-2 ansible_host=${aws_instance.kube_node.2.private_ip}


[k8s-input]
input-0

[k8s-output]
output-0

[k8s-repository]
repository-0

[kube-master]
master-0
master-1
master-2

[kube-node]
node-0
node-1
node-2

[etcd:children]
kube-master

[k8s-cluster:children]
kube-master
kube-node
EOF
}

output "ssh_config" {
  value = <<EOF

Host input-0
  Hostname "${aws_instance.input_host.public_ip}"

Host ${replace(data.terraform_remote_state.network.vpc_cidr, "0.0/16", "*")}
  ProxyCommand ssh -F ./ssh.${local.init_tags["basename"]}.cfg -W %h:%p input-0

Host *
  IdentityFile inventory/${local.init_tags["basename"]}/${local.init_tags["basename"]}.master-key
  StrictHostKeyChecking False
  User centos

EOF
}