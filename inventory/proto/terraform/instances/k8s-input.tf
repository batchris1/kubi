#---------------------------------------------
# CREATE A KEYPAIR
#---------------------------------------------

resource "aws_key_pair" "key" {
  key_name   = "${local.init_tags["name"]}-input"
  public_key = "${file(var.public_key_path)}"
}

resource "aws_instance" "input_host" {
  ami           = "${data.aws_ami.centos.image_id}"
  instance_type = "${var.input_instance_type}"
  key_name      = "${aws_key_pair.key.key_name}"

  vpc_security_group_ids = ["${aws_security_group.input_host.id}"]

  subnet_id = "${data.terraform_remote_state.network.public_subnet_id_a}"

  associate_public_ip_address = true

  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = "10"
  }

  tags {
    Name        = "${local.init_tags["name"]}-input-0"
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}


resource "aws_security_group" "input_host" {
  vpc_id = "${data.terraform_remote_state.network.vpc_id}"

  name        = "${local.init_tags["name"]}-k8s-input"
  description = "Applied to k8s-input"

  tags {
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}

resource "aws_security_group_rule" "input_host_ssh_in" {
  security_group_id = "${aws_security_group.input_host.id}"

  type      = "ingress"
  protocol  = "tcp"
  from_port = 22
  to_port   = 22

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "input_host_out" {
  security_group_id = "${aws_security_group.input_host.id}"

  type      = "egress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
}
