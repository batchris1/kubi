
resource "aws_instance" "kube_master" {
  count                  = 3

  ami                    = "${data.aws_ami.centos.image_id}"
  instance_type          = "${var.instance_type}"
  key_name               = "${aws_key_pair.key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.k8s_cluster.id}"]
  subnet_id              = "${data.terraform_remote_state.network.private_subnets_id_list[count.index]}"

  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = "10"
  }

  tags {
    Name        = "${local.init_tags["name"]}-kube-master-${count.index}"
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}

resource "aws_instance" "kube_node" {
  count                  = 3

  ami                    = "${data.aws_ami.centos.image_id}"
  instance_type          = "${var.instance_type}"
  key_name               = "${aws_key_pair.key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.k8s_cluster.id}"]
  subnet_id              = "${data.terraform_remote_state.network.private_subnets_id_list[count.index]}"

  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = "10"
  }

  tags {
    Name        = "${local.init_tags["name"]}-kube-node-${count.index}"
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}

resource "aws_ebs_volume" "volumes" {
  count             = 3

  availability_zone = "${var.aws_region}${local.az_list[count.index]}"
  size              = "${var.ceph_volume_size}"

  tags {
    Name        = "${local.init_tags["name"]}-kube-node-${count.index}"
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}

resource "aws_volume_attachment" "ebs_attachement" {
  count        = 3

  device_name  = "${var.ceph_volume_mapping}"
  volume_id    = "${element(aws_ebs_volume.volumes.*.id, count.index)}"
  instance_id  = "${element(aws_instance.kube_node.*.id, count.index)}"
  force_detach = true
}



resource "aws_security_group" "k8s_cluster" {
  vpc_id = "${data.terraform_remote_state.network.vpc_id}"

  name        = "${local.init_tags["name"]}-k8s-cluster"
  description = "Applied to k8s-cluster"

  tags {
    Environment = "${local.init_tags["env"]}"
    Owner       = "${local.init_tags["owner"]}"
    Stack       = "${local.init_tags["stack"]}"
  }
}

resource "aws_security_group_rule" "k8s_cluster_ssh_in" {
  security_group_id = "${aws_security_group.k8s_cluster.id}"

  type      = "ingress"
  protocol  = "tcp"
  from_port = 22
  to_port   = 22

  source_security_group_id = "${aws_security_group.input_host.id}"
}

resource "aws_security_group_rule" "k8s_cluster_internal_in" {
  security_group_id = "${aws_security_group.k8s_cluster.id}"

  type      = "ingress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  source_security_group_id = "${aws_security_group.k8s_cluster.id}"
}

resource "aws_security_group_rule" "k8s_cluster_api_in" {
  security_group_id = "${aws_security_group.k8s_cluster.id}"

  type      = "ingress"
  protocol  = "tcp"
  from_port = 6443
  to_port   = 6443

  source_security_group_id = "${aws_security_group.input_host.id}"
}

resource "aws_security_group_rule" "k8s_cluster_insecure_ingress_in" {
  security_group_id = "${aws_security_group.k8s_cluster.id}"

  type      = "ingress"
  protocol  = "tcp"
  from_port = 31080
  to_port   = 31080

  source_security_group_id = "${aws_security_group.input_host.id}"
}

resource "aws_security_group_rule" "k8s_cluster_secure_ingress_in" {
  security_group_id = "${aws_security_group.k8s_cluster.id}"

  type      = "ingress"
  protocol  = "tcp"
  from_port = 31443
  to_port   = 31443

  source_security_group_id = "${aws_security_group.input_host.id}"
}

resource "aws_security_group_rule" "k8s_cluster_out" {
  security_group_id = "${aws_security_group.k8s_cluster.id}"

  type      = "egress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
}
