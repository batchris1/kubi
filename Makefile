# Based on "direnv" bash addon.
# Example of .envrc :
#
# export CLUSTER=alibaba-default
#

tf-apply-network:
	bash -c "cd inventory/${CLUSTER}/terraform/network && terraform init && terraform apply -auto-approve"

tf-destroy-network:
	bash -c "cd inventory/${CLUSTER}/terraform/network && terraform init && terraform destroy -force"

tf-apply-instances:
	bash -c "cd inventory/${CLUSTER}/terraform/instances && terraform init && terraform apply -auto-approve"

tf-destroy-instances:
	bash -c "cd inventory/${CLUSTER}/terraform/instances && terraform init && terraform destroy -force"

tf-output:
	bash -c "cd inventory/${CLUSTER}/terraform/instances && terraform output ssh_config > ../../../../ssh.${CLUSTER}.cfg"
	bash -c "cd inventory/${CLUSTER}/terraform/instances && terraform output ansible_inventory > ../../hosts.ini"

tf-reset: tf-destroy-instances tf-destroy-network tf-apply-network tf-apply-instances tf-output

install-bootstrap: install-requirements
	ansible-playbook --become integration/platform-bootstrap.yml

install-repository: install-requirements
	ansible-playbook --become integration/repository-install-all.yml

install-kube:
	ansible-playbook --become cluster.yml -e registry_enabled=false
	bash -c "sleep 30"
	ansible-playbook --become integration/platform-install-ingresses.yml

install-input: install-requirements
	ansible-playbook --become integration/input-install-all.yml

install-storage:
	ansible-playbook --become integration/platform-install-storage.yml

install-userspaces:
	ansible-playbook --become integration/ops-create-k8s-userspaces.yml

install-output: install-requirements
	ansible-playbook --become integration/output-install-logs-analyzer.yml
	ansible-playbook --become integration/platform-install-logs-collection.yml
	ansible-playbook --become integration/platform-install-metrics-collection.yml

import-docker-image-to-registry:
	ansible-playbook integration/import-docker-image-to-registry.yml

install-all: install-bootstrap install-kube install-repository install-input install-output install-storage install-userspaces

mitogen:
	ansible-playbook -c local mitogen.yaml -vv -e mitogen_version=v0.2.3

clean:
	rm -rf dist/
	rm *.retry

ssh-input:
	bash -c "ssh -F ssh.${CLUSTER}.cfg -L 0.0.0.0:9000:localhost:80 `cat inventory/${CLUSTER}/hosts.ini | grep ansible_host | grep input-0 | cut -d '=' -f2 | cut -d ' ' -f1`"

ssh-input-echo:
	echo "ssh -F ssh.${CLUSTER}.cfg input1 -L 0.0.0.0:9000:localhost:80"

ssh-output:
	bash -c "ssh -F ssh.${CLUSTER}.cfg `cat inventory/${CLUSTER}/hosts.ini | grep ansible_host | grep output-0 | cut -d '=' -f2 | cut -d ' ' -f1`"

ssh-repository:
	bash -c "ssh -F ssh.${CLUSTER}.cfg `cat inventory/${CLUSTER}/hosts.ini | grep ansible_host | grep repository-0 | cut -d '=' -f2 | cut -d ' ' -f1`"

ssh-master:
	bash -c "ssh -F ssh.${CLUSTER}.cfg `cat inventory/${CLUSTER}/hosts.ini | grep ansible_host | grep master-0 | cut -d '=' -f2 | cut -d ' ' -f1`"

ssh-node:
	bash -c "ssh -F ssh.${CLUSTER}.cfg `cat inventory/${CLUSTER}/hosts.ini | grep ansible_host | grep node-0 | cut -d '=' -f2 | cut -d ' ' -f1`"

install-requirements: mitogen
	ansible-galaxy install -fr requirements.yml

ping:
	ansible -m ping all
